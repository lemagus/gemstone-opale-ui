<!doctype html>
<html class="no-js" lang="en">
   <head> 
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Gemstone Opale</title>

      <!-- links -->
      <link rel="stylesheet" href="stylesheets/app.css" />
      <link rel="stylesheet" href="stylesheets/main.css" />
      <link rel="stylesheet" href="stylesheets/font-awesome.min.css" />
      <link rel="stylesheet" href="bower_components/foundation-datepicker/css/foundation-datepicker.min.css">
      <link rel="stylesheet" href="http://c3js.org/css/c3-b03125fa.css">

      <!-- scripts -->
      <script src="bower_components/modernizr/modernizr.js"></script>
      <script src="//use.typekit.net/xul0rrn.js"></script>
      <script>try{Typekit.load({ async: true });}catch(e){}</script>

      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="http://c3js.org/js/d3-3.5.0.min-3b564392.js" type="text/javascript"></script>
      <script src="http://c3js.org/js/c3.min-12912fb6.js" type="text/javascript"></script>

      <script>
         $(function () {
            window.prettyPrint && prettyPrint();
        });
      </script>
   </head>

   <body>

   <!-- Header -->
      <?php include('html_elements/header.html'); ?>
   <!-- /Header -->


   <!-- Lib elements -->
      <main>

         <div class="row">
            <div class="large-6 columns section-elements title-section">
               <h1><span>1.</span> Title hierarchy</h1>
               <h1>Title 1</h1>
               <h2>Title 2</h2>
               <h3>Title 3</h3>
               <h4>Title 4</h4>
               <h5>Title 5</h5>
               <h6>Title 6 + <span class="highlight">span.highlight</span></h6>
            </div>

            <div class="large-6 columns section-elements">
               <h1><span>2.</span> Text styles</h1>
               <h3>Paragraphs : </h3>
               <p>Paragraph + <a href="#">Link</a></p>
               <p>Paragraph + <span class="highlight">span.highlight</span></p>
               <br>

               <ul>
                  <h3>Lists : </h3>
                  <li>List element</li>
                  <li>List element + <a href="#">Link</a></li>
                  <li>List element + <span class="highlight">span.highlight</span></li>
               </ul>

            </div>

            <div class="large-6 columns section-elements">
               <h1><span>3.</span> Buttons styles</h1>
               <button>Button</button>
               <button class="active">Button.active</button>
               <button class="disabled">Button.disabled</button>
               <button class="error">Button.error</button> 
               <button class="expand">Button.expand</button> 
               <button class="more">Button.more</button> 

            </div>

            <div class="large-6 columns section-elements">
               <h1><span>4.</span> Icons classes</h1>
               <ul>
                  <li><i class="fa fa-home"></i><span> .fa-home</span></li>
                  <li><i class="fa fa-tachometer"></i><span> .fa-tachometer</span></li>
                  <li><i class="fa fa-heart"></i><span> .fa-heart</span></li>
                  <li>... <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank">Font Awesome</a></li>
               </ul>
            </div>

            <div class="large-12 columns section-elements">
               <h1><span>5.</span> Tooltips</h1>

               <div class="row">
                  <div class="large-4 columns">
                     <h5>Tooltip (chart campaign)</h5>
      
                     <div class="tooltip-bloc tooltip-chart">
                        <p class="title"><i class="fa fa-bar-chart"></i> Tooltip title</p>
                        <ul class="resume">
                           <li>Facebook Engagement : <span>960</span></li>
                           <li>Companion unique visite : <span>32</span></li>
                           <li>Facebook Likes : <span>16</span></li>
                        </ul>
                     </div>
                  </div>

                  <div class="large-4 columns">
                     <h5>Tooltip info</h5>
                     <div class="tooltip-bloc tooltip-info">
                        <i class="fa fa-info-circle"></i>
                        <p class="resume">
                           Morbi leo risus, porta ac consectetur ac, vestibulum at eros Aenean Ridiculus Egestas + <span>span</span>
                        </p>
                     </div>
                     <i class="fa fa-info-circle tooltip-info-btn"></i><p>(button activation tooltip)</p>
                  </div>


                  <div class="large-4 columns">
                     <h5>Tooltip info</h5>
                     <div class="tooltip-bloc tooltip-reach">
                        <p class="title">Reach legend</p>
                        <ul class="reach">
                           <li><span>1600</span> Organic</li>
                           <li><span>800</span> Paid</li>
                        </ul>
                     </div>
                  </div>
               </div>
               
            </div>
            <div class="large-12 columns section-elements">
               <h1><span>6.</span> Tables</h1>
               <?php include('html_elements/data-table.html'); ?>
            </div>

            <div class="large-12 columns section-elements" style="height=auto;">
               <h1><span>7.</span> Blocs Overview</h1>
                  
                  <div class="row">
                     <div class="large-4 columns">
                        <?php include('html_elements/bloc-overview-fb.html'); ?>
                     </div>

                     <div class="large-4 columns">
                        <?php include('html_elements/bloc-overview-fbads.html'); ?>
                     </div>

                     <div class="large-4 columns">
                        <?php include('html_elements/bloc-overview-ga.html'); ?>
                     </div>

                     <div class="large-4 columns">
                        <?php include('html_elements/bloc-overview-mc.html'); ?>
                     </div>
                  </div>

                  <p>Clique sur le bloc : <a href="popup-overview.html">ouvre popup-overview</a></p>

                  <div class="row">
                     <br><br>
                     <h1><span>7.</span> Tableau Overview</h1>
                     <br>

                     <div class="large-8 columns">

                        <table class="overview-table data-table">
                           <thead>
                              <tr>
                                 <th width="2%">&nbsp;</th>
                                 <th width="40%">Name</th>
                                 <th>Users</th>
                                 <th>Global %</th>
                                 <th>Bouce Rate</th>
                                 <th>Influence</th>
                              </tr>
                           </thead>

                           <tbody>
                              <tr>
                                 <td class="type-color analytics">&nbsp;</td>
                                 <td>Source name</td>
                                 <td>458</td>
                                 <td>30%</td>
                                 <td>35%</td>
                                 <td>00:00:00</td>
                              </tr>
                              <tr>
                                 <td class="type-color facebook">&nbsp;</td>
                                 <td>Source name</td>
                                 <td>458</td>
                                 <td>30%</td>
                                 <td>35%</td>
                                 <td>00:00:00</td>
                              </tr>
                              <tr>
                                 <td class="type-color facebook">&nbsp;</td>
                                 <td>Source name</td>
                                 <td>458</td>
                                 <td>30%</td>
                                 <td>35%</td>
                                 <td>00:00:00</td>
                              </tr>
                              <tr>
                                 <td class="type-color analytics">&nbsp;</td>
                                 <td>Source name</td>
                                 <td>458</td>
                                 <td>30%</td>
                                 <td>35%</td>
                                 <td>00:00:00</td>
                              </tr>
                              <tr>
                                 <td class="type-color facebook">&nbsp;</td>
                                 <td>Source name</td>
                                 <td>458</td>
                                 <td>30%</td>
                                 <td>35%</td>
                                 <td>00:00:00</td>
                              </tr>
                              <tr>
                                 <td class="type-color analytics">&nbsp;</td>
                                 <td>Source name</td>
                                 <td>458</td>
                                 <td>30%</td>
                                 <td>35%</td>
                                 <td>00:00:00</td>
                              </tr>
                           </tbody>

                        </table>
                     </div>
                     <div class="large-4 columns right">
                        <img src="http://i.imgur.com/r1K4Kk8.png?1" width="78%" style="float:right;">
                     </div>          
                  </div>

                  <div class="row">
                     <br><br>
                     <h1><span>7.</span> Tableau Overview - facebook</h1>
                     <br>

                     <div class="large-8 columns">

                        <table class="overview-table-facebook data-table">
                           <thead>
                              <tr>
                                 <th width="10%">&nbsp;</th>
                                 <th width="50%">Description</th>
                                 <th>Paid</th>
                                 <th>Organic</th>
                                 <th width="70px">Likes</th>
                                 <th width="70px">Comments</th>
                                 <th width="70px">Share</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td><img src="https://placeimg.com/100/100/animals"></td>
                                 <td>Vestibulum id ligula porta felis euismod semper. Curabitur blandit tempus porttitor.Donec ullamcorper nulla non metus auctor fringilla.</td>
                                 <td>500</td>
                                 <td>500</td>
                                 <td>500</td>
                                 <td>500</td>
                                 <td>500</td>
                              </tr>
                              <tr>
                                 <td><img src="https://placeimg.com/100/100/animals"></td>
                                 <td>Vestibulum id ligula porta felis euismod semper. Curabitur blandit tempus porttitor.Donec ullamcorper nulla non metus auctor fringilla.</td>
                                 <td>500</td>
                                 <td>500</td>
                                 <td>500</td>
                                 <td>500</td>
                                 <td>500</td>
                              </tr>
                              <tr>
                                 <td><img src="https://placeimg.com/100/100/animals"></td>
                                 <td>Vestibulum id ligula porta felis euismod semper. Curabitur blandit tempus porttitor.Donec ullamcorper nulla non metus auctor fringilla.</td>
                                 <td>500</td>
                                 <td>500</td>
                                 <td>500</td>
                                 <td>500</td>
                                 <td>500</td>
                              </tr>
                              <tr>
                                 <td ><img src="https://placeimg.com/100/100/animals"></td>
                                 <td>Vestibulum id ligula porta felis euismod semper. Curabitur blandit tempus porttitor.Donec ullamcorper nulla non metus auctor fringilla.</td>
                                 <td>500</td>
                                 <td>500</td>
                                 <td>500</td>
                                 <td>500</td>
                                 <td>500</td>
                              </tr>
                              <tr>
                                 <td><img src="https://placeimg.com/100/100/animals"></td>
                                 <td>Vestibulum id ligula porta felis euismod semper. Curabitur blandit tempus porttitor.Donec ullamcorper nulla non metus auctor fringilla.</td>
                                 <td>500</td>
                                 <td>500</td>
                                 <td>500</td>
                                 <td>500</td>
                                 <td>500</td>
                              </tr>
                              <tr>
                                 <td ><img src="https://placeimg.com/100/100/animals"></td>
                                 <td>Vestibulum id ligula porta felis euismod semper. Curabitur blandit tempus porttitor.Donec ullamcorper nulla non metus auctor fringilla.</td>
                                 <td>500</td>
                                 <td>500</td>
                                 <td>500</td>
                                 <td>500</td>
                                 <td>500</td>
                              </tr>
                           </tbody>
                        </table>
                     </div> 

                     <div class="large-4 columns right">
                        <img src="http://i.imgur.com/r1K4Kk8.png?1" width="78%" style="float:right;">
                     </div>
                             
                  </div>

            </div>

            <div class="large-12 columns section-elements">
               <h1><span>8.</span>Overview > Facebook post/ads</h1>
               <div class="row">
                  <div class="large-4 columns">
                     <h5>Facebook Post</h5>
                     <?php include('html_elements/bloc-campaign-fb.html'); ?>
                  </div>
                  <div class="large-4 columns">
                     <h5>Facebook Ads</h5>
                     <?php include('html_elements/bloc-campaign-fbads.html'); ?>
                  </div>
               </div>
            </div>

            <div class="large-12 columns section-elements">
               <h1><span>9.</span>Reporting / blocs</h1>
               
               <div class="row">
                  <div class="large-4 columns">
                    <?php include('html_elements/reporting-bloc.html'); ?>
                  </div>
                  <div class="large-4 columns">
                    <?php include('html_elements/reporting-bloc.html'); ?>
                  </div>
               </div>

               <p>Clique sur le bloc : <a href="popup-reporting.html">ouvre popup-reporting</a></p>

            </div>

            <div class="large-12 columns section-elements">
                <h1><span>10.</span> Blocs Campaigns (popup campaigns)</h1>
                <div class="row">
                    <div class="large-4 columns">
                        <?php include('html_elements/bloc-campaign-fb-popup.html'); ?>
                    </div>
                </div>
            </div>

            <div class="large-12 columns section-elements" style="background: #fff; border:1px solid #cacaca;">
               <h1><span>11.</span> Datepicker pannel</h1>
                    <?php include('html_elements/datepicker.html'); ?>
            </div>

            <div class="large-12 columns section-elements">
               <h1><span>12.</span> Dropdown toggle</h1>
               <?php include('html_elements/accordion.html'); ?>
            </div>

            <div class="large-12 columns section-elements">
               <h1><span>13.</span> Buttons Bar charts Campaigns</h1>
               <?php include('html_elements/top-chart.html'); ?>
            </div>

            <div class="large-12 columns section-elements">
               <h1><span>14.</span> Login bloc</h1>
               <a href="page-login.html">Voir la page login</a>
               
            </div>

            <div class="large-12 columns section-elements">
               <h1><span>15.</span> Loader</h1>
               <a href="popup-loading.html">Voir la popup loader</a>
            </div>

            <div class="large-12 columns section-elements">
               <h1><span>16.</span> Export panel</h1>
               <?php include('html_elements/export-panel.html'); ?>
            </div>

            <div class="large-12 columns section-elements">
               <h1><span>16.</span> Show more</h1>              
               <div class="row">
                <div class="section-height-limiter">
                  <div class="medium-4 columns"><?php include('html_elements/reporting-bloc.html'); ?></div>
                  <div class="medium-4 columns"><?php include('html_elements/reporting-bloc.html'); ?></div>
                  <div class="medium-4 columns"><?php include('html_elements/reporting-bloc.html'); ?></div>
                  <div class="medium-4 columns"><?php include('html_elements/reporting-bloc.html'); ?></div>
                  <div class="medium-4 columns"><?php include('html_elements/reporting-bloc.html'); ?></div>
                  <div class="medium-4 columns"><?php include('html_elements/reporting-bloc.html'); ?></div>
                  <div class="medium-4 columns"><?php include('html_elements/reporting-bloc.html'); ?></div>
                  <div class="medium-4 columns"><?php include('html_elements/reporting-bloc.html'); ?></div>
                  <div class="medium-4 columns"><?php include('html_elements/reporting-bloc.html'); ?></div>
                  <div class="medium-4 columns"><?php include('html_elements/reporting-bloc.html'); ?></div>
                  <div class="medium-4 columns"><?php include('html_elements/reporting-bloc.html'); ?></div>
                  <div class="medium-4 columns"><?php include('html_elements/reporting-bloc.html'); ?></div>
                  <div class="medium-4 columns"><?php include('html_elements/reporting-bloc.html'); ?></div>
                  <div class="medium-4 columns"><?php include('html_elements/reporting-bloc.html'); ?></div>
                  <div class="medium-4 columns"><?php include('html_elements/reporting-bloc.html'); ?></div>
                </div>
               </div>
            </div>

            <div class="large-12 columns section-elements">
                <h1><span>16.</span> Campaign details analytics</h1>              
                <div class="row">                 
                     <div class="large-4 columns">
                        <?php include('html_elements/bloc-campaign-analytics.html'); ?>
                     </div>
                     <div class="large-4 columns">
                        <?php include('html_elements/bloc-campaign-analytics.html'); ?>
                     </div>
                     <div class="large-4 columns">
                        <?php include('html_elements/bloc-campaign-analytics.html'); ?>
                     </div>
                </div>

            </div>

            <div class="large-12 columns section-elements">
                <h1><span>17.</span> Legend Options Chart</h1>              
                <div class="row">                 
                     <ul class="chart-options">
                        <li><i class="fa fa-square orange"></i> Moulinex.be: unique visits</li>
                        <li><i class="fa fa-square orange"></i> Moulinex.be: unique visits</li>
                        <li><i class="fa fa-square blue"></i> Moulinex.be: unique visits</li>
                        <li><i class="fa fa-square orange"></i> Moulinex.be: unique visits</li>
                     </ul>
                </div>

            </div>

            <div class="large-12 columns section-elements">
               <h1><span>18.</span> Action Panel</h1>              
                
               <div class="row">                 
                  <div class="large-6 columns">
                     <div class="bloc-targets-engage">
                        <h3>Date range <i class="fa fa-info-circle"></i></h3>
                        <div class="datepicker-target">
                           <?php include('html_elements/datepicker.html'); ?>
                        </div>
                        <?php include('html_elements/export-panel.html'); ?>

                     </div>
                  </div>

                  <div class="large-6 columns">
                     <div class="bloc-targets-venndiagram">

                        <ul class="tabs" data-tab>
                           <li class="tab-title active"><a href="#panel-leads">Leads</a></li>
                           <li class="tab-title"><a href="#panel-buyers">Buyers</a></li>
                           <li class="tab-title"><a href="#panel-all">All</a></li>
                        </ul>

                        <div class="tabs-content">
                           <div class="content active" id="panel-leads">
                              <div class="bloc-diagram">
                                 <button>Sort by <i class="fa fa-angle-down"></i></button>
                                 <div class="pie-chart">
                                    <span>venn diagram</span>
                                 </div>
                              </div>
                           </div>

                           <div class="content" id="panel-buyers">
                              <div class="bloc-diagram">
                                 <button>Sort by <i class="fa fa-angle-down"></i></button>
                                 <div class="pie-chart">
                                    <span>venn diagram</span>
                                 </div>
                              </div>
                           </div>

                           <div class="content" id="panel-all">
                              <div class="bloc-diagram">
                                 <button>Sort by <i class="fa fa-angle-down"></i></button>
                                 <h3>Leads</h3>
                                 <div class="pie-chart">
                                    <span>venn diagram</span>
                                 </div>
                              </div>

                              <div class="bloc-diagram">
                                 <button>Sort by <i class="fa fa-angle-down"></i></button>
                                 <h3>Buyers</h3>
                                 <div class="pie-chart">
                                    <span>venn diagram</span>
                                 </div>
                              </div>
                              
                           </div>
                        </div>
                        
                     </div>
                  </div>
               </div>

            </div>


      </main>
   <!-- / lib elements -->
     
    <script src="bower_components/foundation/js/foundation.min.js"></script>
    <script src="js/app.js"></script>
    <script src="bower_components/foundation-datepicker/js/foundation-datepicker.js"></script>
    <script src="js/datepicker.js"></script>
    <script src="js/section-height-limiter.js"></script>
      
   </body>
</html>
