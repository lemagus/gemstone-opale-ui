$(function () {
    //section-height-limiter - Show More
    var section_height_limiter_list = $('.section-height-limiter');
    section_height_limiter_list.on('click', '.section-height-limiter__more-btn', function(e) {
        e.preventDefault();
        $(this).parent('.section-height-limiter').addClass('section-height-limit--open');
    });
    section_height_limiter_list.each(function(i, el) {
        console.log(el);
        var client_height = el.clientHeight;
        var scroll_height = el.scrollHeight;
        if(client_height !== scroll_height) {
            //overflow!
            $(el).append('<button class="more section-height-limiter__more-btn">More</button>');
        } else {
            //no overflow, set open state
            $(el).addClass('section-height-limit--open');
        }
    });
});