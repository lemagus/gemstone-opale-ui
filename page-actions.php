<!doctype html>
<html class="no-js" lang="en">
   <head> 
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Gemstone Opale</title>
      <!-- links -->
      <link rel="stylesheet" href="stylesheets/app.css" />
      <link rel="stylesheet" href="stylesheets/main.css" />
      <link rel="stylesheet" href="stylesheets/font-awesome.min.css" />
      <link rel="stylesheet" href="bower_components/foundation-datepicker/css/foundation-datepicker.min.css">
      <link rel="stylesheet" href="http://c3js.org/css/c3-b03125fa.css">
      <!-- scripts -->
      <script src="bower_components/modernizr/modernizr.js"></script>
      <script src="//use.typekit.net/xul0rrn.js"></script>
      <script>try{Typekit.load();}catch(e){}</script>

      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="http://c3js.org/js/d3-3.5.0.min-3b564392.js" type="text/javascript"></script>
      <script src="http://c3js.org/js/c3.min-12912fb6.js" type="text/javascript"></script>
      

      <script>
         $(function () {
            window.prettyPrint && prettyPrint();
        });
      </script>
   </head>

   <body class="">

        <!-- Header -->
        <header>
           <a href="/" class="main-back-btn"><i class="fa fa-arrow-left"></i></a>
           <p>Groupe Seb Belgium</p>
           <div class="right">
                <a href="/analytics/" class=""><i class="fa fa-home"></i> Dashboard</a>
                <a href="/actions/" class="active"><i class="fa fa-thumb-tack"></i> Actions</a>
                <a href="/login/logout/"><i class="fa fa-power-off"></i> Logout</a>
                <a href="http://www.adjust.be" target="_blank" class="icon icon-adjust-girafe"></a>
           </div>
        </header>

            
        <main>
            <!-- main content goes here -->
            <div class="row full-width actions-page">
                <div class="large-12 columns">

                    <h1>Actions</h1>

                    <dl id="actions-page-acc" class="accordion" data-accordion="" aria-multiselectable="true">
                        <dd class="accordion-navigation section-panel main-accordion-item">
                            <a href="#actions" class="main-accordion-link" aria-expanded="true">
                                <i class="fa fa-arrows-v"></i> Summary <i class="fa fa-plus-circle"></i>
                            </a>
                            <div id="actions" class="content" data-need="summary">
                                <div class="row">
                                    <div class="large-4 columns"><?php include('html_elements/action-bloc.html'); ?></div>
                                    <div class="large-4 columns"><?php include('html_elements/action-bloc.html'); ?></div>
                                    <div class="large-4 columns"><?php include('html_elements/action-bloc.html'); ?></div>
                                    <div class="large-4 columns"><?php include('html_elements/action-bloc.html'); ?></div>
                                    <div class="large-4 columns"><?php include('html_elements/action-bloc.html'); ?></div>
                                    <div class="large-4 columns"><?php include('html_elements/action-bloc.html'); ?></div>
                                    <div class="large-4 columns"><?php include('html_elements/action-bloc.html'); ?></div>
                                </div>
                            </div>

                        </dd>

                        <dd class="accordion-navigation section-panel main-accordion-item">
                            <a href="#bar-chart" class="main-accordion-link" aria-expanded="true">
                                <i class="fa fa-arrows-v"></i> Bar chart <i class="fa fa-plus-circle"></i>
                            </a>
                            <div id="bar-chart" class="content" data-need="aggregate">
                                <?php include('html_elements/actions-bar-chart.html'); ?>
                            </div>
                        </dd>

                        <dd class="accordion-navigation section-panel main-accordion-item">
                            <a href="#venn-chart" class="main-accordion-link" aria-expanded="true">
                                <i class="fa fa-arrows-v"></i> Venn Diagram <i class="fa fa-plus-circle"></i>
                            </a>
                            <div id="venn-chart" class="content" data-need="venn">
                                <?php include('html_elements/actions-venn.html'); ?>
                            </div>
                        </dd>
        
                        <dd class="accordion-navigation section-panel main-accordion-item">
                            <a href="#venn-chart-analytics" class="main-accordion-link" aria-expanded="true">
                                <i class="fa fa-arrows-v"></i> Venn Diagram - Evolution analysis <i class="fa fa-plus-circle"></i>
                            </a>
                            <div id="venn-chart-analytics" class="content disabled" data-need="venn-analysis">
                                <?php include('html_elements/actions-venn-analytics.html'); ?>
                            </div>
                        </dd>

                        <dd class="accordion-navigation section-panel main-accordion-item">
                            <a href="#actions-list" class="main-accordion-link" aria-expanded="true">
                                <i class="fa fa-arrows-v"></i> Leads <i class="fa fa-plus-circle"></i>
                            </a>
                            <div id="actions-list" class="content" data-need="actions">
                                <?php include('html_elements/actions-leads-table.html'); ?>
                            </div>
                        </dd>

                    </dl>

    
                </div>
            </div>
        </main>

        <script src="bower_components/foundation/js/foundation.min.js"></script>
        <script src="js/app.js"></script>
        <script src="bower_components/foundation-datepicker/js/foundation-datepicker.js"></script>
        <script src="js/datepicker.js"></script>
        <script src="js/section-height-limiter.js"></script>    

    </body>
</html>

   

