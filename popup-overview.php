<!doctype html>
<html class="no-js" lang="en">
   <head> 
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Gemstone Opale</title>

      <link rel="stylesheet" href="stylesheets/app.css" />
      <link rel="stylesheet" href="stylesheets/main.css" />

      <link rel="stylesheet" href="stylesheets/font-awesome.min.css" />

      <script src="bower_components/modernizr/modernizr.js"></script>

      <script src="//use.typekit.net/xul0rrn.js"></script>
      <script>try{Typekit.load();}catch(e){}</script>
   </head>

   <body>

      <div class="popup popup-overview">

         <a class="close-popup"><i class="fa fa-times"></i></a>

         <div class="row">
            <div class="large-12 columns">
               <h3>Facebook: Engagement</h3>

               <div class="row">
                  <div class="large-4 columns right">
                     <?php include("html_elements/datepicker.html"); ?>  
                  </div>
               </div>  

               <div class="row">
                  <div class="large-8 columns">
                     <div class="overview-detail">
                        <p class="title">Engagement :</p>
                        <p>Facebook engagement <span>400</span></p>
                        <p>Likes, comments, shares <span>350</span></p>
                     </div>
                  </div>
                  <div class="large-4 columns">
                     <div class="overview-reach">
                        <p>Reach <span>73000</span></p>
                        <ul>
                           <li>Paid <span>56000</span></li>
                           <li>Organic <span>17000</span></li>
                        </ul>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="large-12 columns">
                     <form class="overview-hashtags">
                        <h4>Talk to your likers :</h4>
                        <ul class="hashtags-list">
                           <li>
                              <a class="hashtags">All likers</a>
                              <span>50000 people</span>
                              <input type="checkbox" checked="checked">
                           </li>
                           <li>
                              <a class="hashtags">#IloveFresh</a>
                              <span>50 people</span>
                              <input type="checkbox">
                           </li>
                           <li>
                              <a class="hashtags">#IloveFresh</a>
                              <span>50 people</span>
                              <input type="checkbox">
                           </li>
                           <li>
                              <a class="hashtags">#IloveFresh</a>
                              <span>50 people</span>
                              <input type="checkbox">
                           </li>
                        </ul>

                        <div class="export-buttons">
                           <button class="export-hashtag export-csv"><i class="fa fa-file-excel-o"></i> Export csv</button>
                           <p>or</p>
                           <input type="text" placeholder="List Name">
                           <button class="export-hashtag export-mailchimp"><i class="fa fa-envelope-o"></i> Export Mailchimp</button>
                        </div>
                     </form>
                     
                  </div>
               </div>

               <!-- <div class="row">
                  <div class="large-12 columns">
                     <div class="overview-export">
                        <h4>Export panel :</h4>
                        <?php include("html_elements/export-panel.html"); ?>
                     </div>
                  </div>
               </div> -->
               
               <div class="row">
                  <div class="large-12 columns">
                     <div class="overview-posts">
                        <h4>Posts :</h4>
                        <div class="row">
                           <div class="large-6 columns">
                              <?php include("html_elements/bloc-campaign-fb-popup.html"); ?>
                           </div>
                           <div class="large-6 columns">
                              <?php include("html_elements/bloc-campaign-fb-popup.html"); ?>
                           </div>
                           <div class="large-6 columns">
                              <?php include("html_elements/bloc-campaign-fb-popup.html"); ?>
                           </div>
                           <div class="large-6 columns">
                              <?php include("html_elements/bloc-campaign-fb-popup.html"); ?>
                           </div>
                        </div>
                     </div>
                  </div>

               </div>
            </div>
         </div>

      </div>
     

      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="bower_components/foundation/js/foundation.min.js"></script>
      <script src="js/app.js"></script>
   </body>
</html>
