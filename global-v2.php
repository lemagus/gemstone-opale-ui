<!doctype html>
<html class="no-js" lang="en">
   <head> 
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Gemstone Opale</title>

      <link rel="stylesheet" href="stylesheets/app.css" />
      <link rel="stylesheet" href="stylesheets/main.css" />
      <link rel="stylesheet" href="stylesheets/font-awesome.min.css" />

      <script src="bower_components/modernizr/modernizr.js"></script>
      <script src="//use.typekit.net/xul0rrn.js"></script>
      <script>try{Typekit.load();}catch(e){}</script>
   </head>

   <body>

      <div class="row">

         <div class="section-overview">

            <div class="large-12 columns">
               
               <h3>Overview</h3>

               <?php include('html_elements/datepicker.html'); ?>

               <?php include('html_elements/top-chart.html'); ?>
               <img src="http://i.imgur.com/knzezuC.png" class="chart-full">

               <div class="row">
                  <div class="large-9 columns">
                     <ul class="overview-datas">

                        <li class="opened">
                           <h4>Facebook <span>132273 <span class="percentage">(49% / +101%)</span></span></h4>
                           <div class="bar-container"><span class="bar facebook">&nbsp;</span></div>

                           <ul class="opened-overview-data">
                              <li>
                                 <h4>Engagement <span>132273 <span class="percentage">(49% / +101%)</span></span></h4>
                                 <div class="bar-container"><span class="bar facebook">&nbsp;</span></div>
                              </li>
                              <li>
                                 <h4>Likes <span>132273 <span class="percentage">(49% / +101%)</span></span></h4>
                                 <div class="bar-container"><span class="bar facebook">&nbsp;</span></div>
                              </li>
                              <li>
                                 <h4>Shares <span>132273 <span class="percentage">(49% / +101%)</span></span></h4>
                                 <div class="bar-container"><span class="bar facebook">&nbsp;</span></div>
                              </li>
                           </ul>

                        </li>

                        <li class="opened">
                           <h4>Moulinex.be <span>132273 <span class="percentage">(49% / +101%)</span></span></h4>
                           <div class="bar-container"><span class="bar analytics">&nbsp;</span></div>

                           <ul class="opened-overview-data">
                              <li>
                                 <h4>Users <span>132273 <span class="percentage">(49% / +101%)</span></span></h4>
                                 <div class="bar-container"><span class="bar analytics">&nbsp;</span></div>
                              </li>
                              <li>
                                 <h4>Sessions <span>132273 <span class="percentage">(49% / +101%)</span></span></h4>
                                 <div class="bar-container"><span class="bar analytics">&nbsp;</span></div>
                              </li>
                              <li>
                                 <h4>Shares <span>132273 <span class="percentage">(49% / +101%)</span></span></h4>
                                 <div class="bar-container"><span class="bar analytics">&nbsp;</span></div>
                              </li>
                           </ul>

                        </li>
                        <li class="opened">
                           <h4>Cookeo.be <span>132273 <span class="percentage">(49% / +101%)</span></span></h4>
                           <div class="bar-container"><span class="bar analytics">&nbsp;</span></div>

                           <ul class="opened-overview-data">
                              <li>
                                 <h4>Users <span>132273 <span class="percentage">(49% / +101%)</span></span></h4>
                                 <div class="bar-container"><span class="bar analytics">&nbsp;</span></div>
                              </li>
                              <li>
                                 <h4>Sessions <span>132273 <span class="percentage">(49% / +101%)</span></span></h4>
                                 <div class="bar-container"><span class="bar analytics">&nbsp;</span></div>
                              </li>
                              <li>
                                 <h4>Shares <span>132273 <span class="percentage">(49% / +101%)</span></span></h4>
                                 <div class="bar-container"><span class="bar analytics">&nbsp;</span></div>
                              </li>
                           </ul>

                        </li>
                        <li class="opened">
                           <h4>Analytics <span>132273 <span class="percentage">(49% / +101%)</span></span></h4>
                           <div class="bar-container"><span class="bar analytics">&nbsp;</span></div>

                           <ul class="opened-overview-data">
                              <li>
                                 <h4>Users <span>132273 <span class="percentage">(49% / +101%)</span></span></h4>
                                 <div class="bar-container"><span class="bar analytics">&nbsp;</span></div>
                              </li>
                              <li>
                                 <h4>Sessions <span>132273 <span class="percentage">(49% / +101%)</span></span></h4>
                                 <div class="bar-container"><span class="bar analytics">&nbsp;</span></div>
                              </li>
                              <li>
                                 <h4>Shares <span>132273 <span class="percentage">(49% / +101%)</span></span></h4>
                                 <div class="bar-container"><span class="bar analytics">&nbsp;</span></div>
                              </li>
                           </ul>

                        </li>
                     </ul>
                  </div>

                  <div class="large-3 columns right">
                        
                        <table class="overview-ranking">
                           <h4>Ranking</h4>
                           <thead>
                              <tr>
                                 <th width="200">Average</th> 
                                 <th width="200">Position</th> 
                                 <th width="200">Evolution</th>  
                              </tr>
                           </thead>

                           <tbody>
                              <tr>
                                 <td>Cookeo</td>
                                 <td>2</td>
                                 <td>+3</td>
                              </tr>
                              <tr>
                                 <td>Cookeo</td>
                                 <td>2</td>
                                 <td>+3</td>
                              </tr>
                              <tr>
                                 <td>Cookeo</td>
                                 <td>2</td>
                                 <td>+3</td>
                              </tr>
                           </tbody>
                        </table>

                        <button class="more-ranking expand button">More</button>
                     
                  </div>
               </div>
            </div>

         </div>

      </div>

      
      <div class="row">
         <div class="large-12 columns">
            <?php include('html_elements/accordion.html'); ?>
         </div>
      </div>
      
     

      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="bower_components/foundation/js/foundation.min.js"></script>
      <script src="js/app.js"></script>
   </body>
</html>
