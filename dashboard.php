<!doctype html>
<html class="no-js" lang="en">
   <head> 
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Gemstone Opale</title>
      <!-- links -->
      <link rel="stylesheet" href="stylesheets/app.css" />
      <link rel="stylesheet" href="stylesheets/main.css" />
      <link rel="stylesheet" href="stylesheets/font-awesome.min.css" />
      <link rel="stylesheet" href="bower_components/foundation-datepicker/css/foundation-datepicker.min.css">
      <link rel="stylesheet" href="http://c3js.org/css/c3-b03125fa.css">
      <!-- scripts -->
      <script src="bower_components/modernizr/modernizr.js"></script>
      <script src="//use.typekit.net/xul0rrn.js"></script>
      <script>try{Typekit.load({ async: true });}catch(e){}</script>

      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="http://c3js.org/js/d3-3.5.0.min-3b564392.js" type="text/javascript"></script>
      <script src="http://c3js.org/js/c3.min-12912fb6.js" type="text/javascript"></script>
      

      <script>
         $(function () {
            window.prettyPrint && prettyPrint();
        });
      </script>
   </head>

   <body>

   <!-- Header -->
      <?php include('html_elements/header.html'); ?>
   <!-- /Header -->


   <!-- Lib elements -->
      <main>


<div class="row full-width">
 <div class="container-analytics large-12 columns">
 <!-- Search by -->
    <section class="search-campaign">
       <form id="search-form" class="analytics-formtop-board">
          <div class="row">
            <div class="large-offset-8 medium-offset-6 large-4 medium-6 small-12 columns">
                <?php include('html_elements/datepicker.html'); ?>
            </div>
          </div>
       </form>
    </section>

 <!-- Campaigns -->
    <section class="campaigns section-panel">
       <div class="row">
          <div id="top-chart-area" class="large-12 columns">
            <?php include('html_elements/top-chart.html'); ?>
          </div>
       </div>
    </section>


 <!-- ////////  ACCORDION GLOBAL START ///////// -->
    <dl id="analytics-accordion" class="accordion" data-accordion>
    
        <!-- Percentage stats -->
       <dd class="accordion-navigation section-panel main-accordion-item" data-item="overview">
          <a href="#panel-percentage" class="main-accordion-link">
            <i class="fa fa-arrows-v"></i> Overview <i class="fa fa-plus-circle"></i>
            <div class="main-accordion-loading"></div>
          </a>
          <div id="panel-percentage" class="content" data-need="history">
             <div class="row">
                <div id="blocs-contener" class="large-8 small-12 columns">
                    <div class="medium-6 columns">
                        <?php include('html_elements/bloc-overview-fb.html'); ?>
                    </div>
                    <div class="medium-6 columns">
                        <?php include('html_elements/bloc-overview-ga.html'); ?>
                    </div>
                </div>
                <div class="large-4 small-12 columns">
                    <div id="pct-pie" style="width:320px;height:320px" ></div>
                </div>
            </div>
            <div class="main-accordion-content-loading"></div>
          </div>
       </dd>

   

    <!-- Campaigns Details -->
       <dd class="accordion-navigation section-panel main-accordion-item" data-item="campaigns-details">
          <a href="#campaigns-details" class="main-accordion-link">
            <i class="fa fa-arrows-v"></i> Campaigns details <i class="fa fa-plus-circle"></i>
          </a>
          <div id="campaigns-details" class="content" data-need="splits">
                <div class="row">
                    <div class="small-12  medium-offset-8 medium-4 large-offset-9 large-3 columns">
                        <select name="assigments" class="assignment-select" >
                                <optgroup label="ga">
                                    <option value="ga" data-service="ga" >Balade: Stats</option>
                                    <option value="ga" data-service="ga" >Balade: Stats</option>
                                </optgroup>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="section-height-limiter large-9 columns">
                        <div id="originCompare" class="row">
                            <div class="large-6 columns">
                                <?php include('html_elements/bloc-campaign-fbads.html'); ?>
                            </div>
                            <div class="large-6 columns">
                                <?php include('html_elements/bloc-campaign-analytics.html'); ?>
                            </div>
                            <div class="large-6 columns">
                                <?php include('html_elements/bloc-campaign-fb.html'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="large-3 columns">
                        <div id="originPie" style="width:247px;height:247px" ></div>
                    </div>
                </div>

          </div>
       </dd>
    
    <!-- Reporting -->
       <dd class="accordion-navigation section-panel main-accordion-item" data-item="reports">
          <a href="#reporting" class="main-accordion-link">
            <i class="fa fa-arrows-v"></i> Reporting <i class="fa fa-plus-circle"></i>
          </a>
          <div id="reporting" class="content"  data-need="reports">
                <div id="reports" class="row">
                    
                    <div class="large-4 columns">
                        <?php include('html_elements/reporting-bloc.html'); ?>
                    </div>
                    <div class="large-4 columns">
                        <?php include('html_elements/reporting-bloc.html'); ?>
                    </div>
                </div> 
          </div>
       </dd>

    <!-- Actions -->
       <dd class="accordion-navigation section-panel main-accordion-item" data-item="actions">
          <a href="#actions" class="main-accordion-link">
            <i class="fa fa-arrows-v"></i> Actions <i class="fa fa-plus-circle"></i>
          </a>
          <div id="actions" class="content" data-need="actions">
                <div class="row">
                    <div class="large-4 columns">
                        <?php include('html_elements/action-bloc.html'); ?>
                    </div>
                </div>
          </div>
       </dd>



    
    </dl>
    <!-- ////////  ACCORDION GLOBAL END ///////// -->
 
 </div><!--/ large-12-->
</div><!--/ row-->

        
      </main>
   <!-- / lib elements -->
     
    <script src="bower_components/foundation/js/foundation.min.js"></script>
    <script src="js/app.js"></script>
    <script src="bower_components/foundation-datepicker/js/foundation-datepicker.js"></script>
    <script src="js/datepicker.js"></script>
    <script src="js/section-height-limiter.js"></script>

   </body>
</html>
