<!doctype html>
<html class="no-js" lang="en">
   <head> 
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Gemstone Opale</title>

      <link rel="stylesheet" href="stylesheets/app.css" />
      <link rel="stylesheet" href="stylesheets/main.css" />

      <link rel="stylesheet" href="stylesheets/font-awesome.min.css" />

      <script src="bower_components/modernizr/modernizr.js"></script>

      <script src="//use.typekit.net/xul0rrn.js"></script>
      <script>try{Typekit.load();}catch(e){}</script>
   </head>

   <body>

      <div class="popup popup-overview">

         <a class="close-popup"><i class="fa fa-times"></i></a>

         <div class="row">
            <div class="large-12 columns">
               <h3>Traffic origins</h3>

               <div class="row">
                  <div class="large-4 columns right">
                     <?php include("html_elements/datepicker.html"); ?>  
                  </div>
               </div>  

               <div class="row">
                  <div class="large-8 columns">
                     <div class="overview-detail">
                        <p class="title">Visits :</p>
                        <p>Unique visits <span>400</span></p>
                        <p>New visitors <span>350</span></p>
                     </div>
                  </div>
                  <div class="large-4 columns">
                     <div class="overview-reach">
                        <p>Behaviour :</p>
                        <ul>
                           <li>Time <span>00:00:00</span></li>
                           <li>Pages <span>1579</span></li>
                        </ul>
                     </div>
                  </div>
               </div>
               
               <div class="row">
                  <div class="large-12 columns">
                     <div class="overview-posts">
                        <h4>All traffic origins :</h4>
                        <div class="row">
                           <div class="large-6 columns">
                              <?php include("html_elements/bloc-campaign-analytics.html"); ?>
                           </div>
                           <div class="large-6 columns">
                              <?php include("html_elements/bloc-campaign-analytics.html"); ?>
                           </div>
                           <div class="large-6 columns">
                              <?php include("html_elements/bloc-campaign-analytics.html"); ?>
                           </div>
                           <div class="large-6 columns">
                              <?php include("html_elements/bloc-campaign-analytics.html"); ?>
                           </div>
                        </div>
                     </div>
                  </div>

               </div>
            </div>
         </div>

      </div>
     

      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="bower_components/foundation/js/foundation.min.js"></script>
      <script src="js/app.js"></script>
   </body>
</html>
